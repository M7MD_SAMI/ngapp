import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { APIs, Service } from 'src/app/services/shard';
import { Users } from '../../interfaces';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  user?: Users;
  userId?:any

  constructor(
    private _service: Service,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe( params => this.userId = params.id );
  }

  ngOnInit(): void {
    this.getUser(this.userId)
  }

  async getUser(id:number){
    this._service.getRequest(APIs.init(id).user).subscribe((res:any) => {
      this.user = res?.data;
      // console.log(this.user)
    })
  }

}

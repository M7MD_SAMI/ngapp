import { Component, OnInit } from '@angular/core';
import { Service, APIs } from '../../services/shard'
import { Users } from '../../interfaces';
import { HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateComponent } from '../create/create.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users?: Users[];
  totalPages?: number[];
  toast?: boolean | string = false;

  constructor(
    private _service: Service,
    private router: Router,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.getUsers('1')
  }

  getUsers(page: string) {
    let params = new HttpParams();
    params = params.append('page', page);
    this._service.getRequest(APIs.init().users, params).subscribe((res: any) => {
      this.users = res?.data;
      this.totalPages = new Array(res?.total_pages);
      // console.log(res)
    })
  }

  nextPage(e: Event, i: number) {
    // e.preventDefault();
    this.getUsers(i + '')
  }

  add() {
    const modalRef = this.modalService.open(CreateComponent);
    modalRef.componentInstance.data = { type: 'add' };
  }

  edit(id: number, i: number) {
    const modalRef = this.modalService.open(CreateComponent);
    modalRef.componentInstance.data = { id, type: 'edit' };
  }

  delete(id: number, i: number) {
    this._service.deleteRequest(APIs.init(id).user).subscribe(res => {
      this.users?.splice(i, 1);
      this.toast = 'User deleted successfuly';
    })
  }

}

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Service, APIs } from '../../services/shard'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private _service: Service,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  async login(form: NgForm) {
    let body = form.value;
    this._service.postRequest(APIs.init().login, body).subscribe((res:any) => {
      console.log(res);
      localStorage.setItem("token", res.token);
      return this.router.navigate(["/users"]);
    },
    err => {
      console.error(err)
    })
  }

}

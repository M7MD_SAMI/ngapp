import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { APIs, Service } from 'src/app/services/shard';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  @Input() data?: any;
  toast?: boolean | string = false;

  constructor(
    public activeModal: NgbActiveModal,
    private _service: Service) { }

  ngOnInit(): void {
  }

  submit(form: NgForm) {
    let body = form.value;

    if(this.data.type == 'add') {      
      this._service.postRequest(APIs.init().users, body).subscribe((res: any) => {
        this.toast = 'User added successfuly';
      })
    }

    if(this.data.type == 'edit') {     
      this._service.putRequest(APIs.init(this.data.userId).user, body).subscribe((res: any) => {
        // console.log(res)
        this.toast = 'User edited successfuly';
      })
    }
  }

}

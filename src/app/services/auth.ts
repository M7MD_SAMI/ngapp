import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

    async isLogin():Promise<boolean> {
        let token = await localStorage.getItem('token');
        return token ? true : false;
    }

}
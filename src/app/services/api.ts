export const APIs = {

    init(param?:string | number) {

        const BASEURL = 'https://reqres.in/api';
        const API = {
            login: `${BASEURL}/login`,
            register: `${BASEURL}/register`,
            users: `${BASEURL}/users`,
            user: `${BASEURL}/users/${param}`,
        }
        return API;
    }
}
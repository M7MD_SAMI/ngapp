import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class Service {

    constructor(
        private _Http: HttpClient
    ) { }

    getRequest<T>(url: string, params?:HttpParams ): Observable<T> {
        if (params) {
            return this._Http.get<T>(url, {params});
        }
        return this._Http.get<T>(url);
    }

    postRequest<T>(url: string, body: {}): Observable<T> {
        return this._Http.post<T>(url, body);
    }

    putRequest<T>(url: string, body: {}): Observable<T> {
        return this._Http.put<T>(url, body);
    }

    deleteRequest<T>(url: string): Observable<T> {
        return this._Http.delete<T>(url);
    }

}
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.log(this.authService.isLogin())
    return new Promise(resolve =>
      this.authService.isLogin()
        .then(status => {
          if (status === false) {
            this.router.navigate(["login"]);
          }
          resolve(status);
        })
        .catch(() => {
          this.router.navigate(["login"]);
          resolve(false);
          // ... or any other way you want to handle such error case
        })
    )
  }

}
